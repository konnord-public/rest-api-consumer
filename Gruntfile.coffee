# VARIABLE ##############################################################

# -- GRUNT DEPENDANCIE --------------------------------------------------
fs = require "fs"

# -- GRUNT --------------------------------------------------------------

grunt = {}

## - TASK ---------------------------------------------------------------

grunt.default = ["ts", "mochaTest"]
grunt.tasks_to_load = []
grunt.load = (grunt_task_package) -> grunt.tasks_to_load.push grunt_task_package

## - VARIABLE -----------------------------------------------------------

grunt.options = {}
grunt.folder = __dirname
grunt.out = grunt.folder + "/target"

# -- PROJECT ------------------------------------------------------------

project = {}
project.package = "fr/rest/api/consumer"

## - SRC OPTIONS --------------------------------------------------------

project.src = {}
project.src.folder = grunt.folder + "/src/" + project.package


## - TEST OPTIONS -------------------------------------------------------

project.test = {}
project.test.folder = grunt.folder + "/test/" + project.package
project.test.log = grunt.out + "/log"

## - ARTEFACT OPTIONS ---------------------------------------------------

project.artefact = {}
project.artefact.folder = grunt.out + "/artefact"
project.artefact.files = []

# -- TEST GRUNT VARIABLE ------------------------------------------------

#
# The variable defined here is reachable by test unit.
#

# FUNCTION ##############################################################

#
# ## list_files
# List all the file from a folder.
# The list includes file from child folder
# if recusif is true.
#
# ### **root [ String ]**
# The root path where we start.
#
# ### **cwd [ String ]**
# actual path where we search file.
#
# ### **recursif [ Boolean ]**
# Say if we must includes the file of child folder.
# The variable is true by default.
#
# ### **Return [ Array [root, cwd of file, file] ]
#
list_files = (root, cwd, recursif) ->

  if recursif == undefined
  then recursif = true

  out = []
  path = root + "/" + cwd
  files = fs.readdirSync path

  files.forEach (file) ->

    cwd_to_file = cwd + "/" + file
    path_to_file = root + "/" + cwd_to_file

    stat = fs.lstatSync path_to_file

    if (is_dir = stat.isDirectory()) && recursif
    then out = out.concat list_files root, cwd_to_file, recursif
    else if !is_dir
    then out.push [root, cwd, file]

  return out

# TASKS #################################################################

# -- BUILD SRC ----------------------------------------------------------
grunt.load "grunt-ts"
grunt.options.ts =
  RestApiConsumer:
    files: []
    options:
      fast: "never"

src_files = list_files project.src.folder, "."
src_files.forEach (tuple) ->
  root = tuple[0]
  cwd = tuple[1]
  file = tuple[2]

  return if !file.endsWith ".ts"

  file_name = file.slice 0, - (".ts").length
  cwd_to_file = "#{ cwd }/#{ file }"
  file_out = "#{ file_name }.js"
  cwd_to_artefact = "#{ cwd }/#{ file_out }"

  # Save the path to artefact file to do extra step with this
  project.artefact.files.push [cwd, file_out]

  # Add an step to generate the file
  grunt.options.ts.RestApiConsumer.files.push {
      src: ["#{ root }/#{ cwd_to_file }"]
      dest: "#{ project.artefact.folder }/#{ cwd_to_artefact }"
  }

# -- ADD TEST UNIT ------------------------------------------------------
grunt.load "grunt-mocha-test"
grunt.options.mochaTest = {}

test_unit = list_files project.test.folder, "."
test_unit.forEach (tuple) ->
  root = tuple[0]
  cwd = tuple[1]
  file = tuple[2]

  return if !file.endsWith ".test.ts"

  group = null
  if (i = cwd.indexOf "/") != -1
  then group = cwd.slice i + 1
  else group = "default"

  if grunt.options.mochaTest[group] == undefined
  then grunt.options.mochaTest[group] =
    options:
      reporter: 'spec'
      captureFile: "#{ project.test.log }/#{ group }.log"
      require: ['ts-node/register']
    src: []

  grunt.options.mochaTest[group].src.push "#{root}/#{cwd}/#{file}"

# -- GRUNT INITIALIZE ---------------------------------------------------

module.exports = (grunt_instance) ->

  grunt_instance.initConfig grunt.options

  grunt.tasks_to_load.forEach (task) ->
    grunt_instance.loadNpmTasks task

  grunt_instance.registerTask "default", grunt.default
